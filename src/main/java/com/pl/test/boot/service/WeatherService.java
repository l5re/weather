package com.pl.test.boot.service;

public interface WeatherService {
  String forecast();
}
