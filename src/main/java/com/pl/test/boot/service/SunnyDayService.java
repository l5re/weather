package com.pl.test.boot.service;

public class SunnyDayService implements WeatherService {

  @Override
  public String forecast() {
    return "Today is sunny day!";
  }

}
