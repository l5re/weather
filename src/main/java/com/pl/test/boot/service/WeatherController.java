package com.pl.test.boot.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {
  
  @RequestMapping("/weather/{town}")
  public String getWeather(@PathVariable String town){
    if ("skopje".equals(town)) {
      return "sunny";
    } else if ("moskva".equals(town)) {
        return "snow";  
    } else {
      return "rain";
    }
    
  }

}
